package ru.iartemov.gjj;

import java.util.*;

public class DuplicateScanner {
    public static boolean containsDuplicatesFirstMethod(int[] numbers) {
        Set set = new HashSet();
        for (int number : numbers) {
            set.add(number);
        }
        return set.size() != numbers.length;
    }

    public static boolean containsDuplicatesSecondMethod(int[] numbers) {
        Set<Integer> set = new HashSet<>();
        for (Integer each: numbers) if (!set.add(each)) return true;
        return false;
    }
}
