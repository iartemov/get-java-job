package ru.iartemov.gjj;

public class Application {

    public static final String TEST_COMPLETE = "Test complete";
    public static final String TEST_FAILED = "Test failed!";

    public static void main(String[] args) {
        int[] test1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        testFirstMethod(test1, false);
        testSecondMethod(test1, false);

        int[] test2 = {1, 2, 3, 4, 5, 6, 7, 8, 1};
        testFirstMethod(test2, true);
        testSecondMethod(test2, true);

        int[] test3 = {1, 2, 3, 4, 5, 6, 7, 8, 1};
        testFirstMethod(test3, true);
        testSecondMethod(test3, true);

        int[] test4 = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        testFirstMethod(test4, false);
        testSecondMethod(test4, false);

        int[] test5 = {9, 8, 7, 6, 5, 4, 3, 2, 9};
        testFirstMethod(test5, true);
        testSecondMethod(test5, true);
    }

    private static void testFirstMethod(int[] methodArg, boolean expectedResult) {
        boolean actualResult = DuplicateScanner.containsDuplicatesFirstMethod(methodArg);
        checkResult(expectedResult, actualResult);
    }

    private static void checkResult(boolean expectedResult, boolean actualResult) {
        if (actualResult == expectedResult) {
            System.out.println(TEST_COMPLETE);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testSecondMethod(int[] methodArg, boolean expectedResult) {
        boolean actualResult = DuplicateScanner.containsDuplicatesSecondMethod(methodArg);
        checkResult(expectedResult, actualResult);
    }
}
