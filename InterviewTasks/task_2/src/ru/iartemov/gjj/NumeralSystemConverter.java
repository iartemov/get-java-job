package ru.iartemov.gjj;

public class NumeralSystemConverter {
    public static long convertToNumberSystem(int value, int numberSystem) {
        checkArgs(value, numberSystem);
        return convertToNumberSystem(value, numberSystem, null);
    }

    private static long convertToNumberSystem(int value, int numberSystem, StringBuilder aggregator) {
        if (aggregator == null) {
            aggregator = new StringBuilder();
        }

        if (value >= numberSystem) {
            aggregator.append(value % numberSystem);
            return convertToNumberSystem(value / numberSystem, numberSystem, aggregator);
        } else {
            aggregator.append(value);
            aggregator.reverse();
            String resultString = aggregator.toString();
            return Long.parseLong(resultString);
        }
    }

    private static void checkArgs(int value, int numberSystem) {
        if (value <= 0) {
            throw new IllegalArgumentException();
        }

        if (numberSystem < 2 || numberSystem > 16) {
            throw new IllegalArgumentException();
        }
    }
}
