package ru.iartemov.gjj;

public class Application {

    public static final String TEST_COMPLETED = "Test completed";
    public static final String TEST_FAILED = "Test failed!";

    public static void main(String[] args) {
        testConvertion(125, 2, 1111101);
        testConvertion(59, 2, 111011);
        testConvertion(23, 2, 10111);

        testConvertion(134, 8, 206);
        testConvertion(85, 8, 125);
        testConvertion(72, 8, 110);

        testConvertion(25, 16, 19);
        testConvertion(68, 16, 44);
        testConvertion(117, 16, 75);

        testIllegalArgumentFailure(-2, 2);
        testIllegalArgumentFailure(2, 0);
        testIllegalArgumentFailure(2, 18);
    }

    private static void testConvertion(int value, int numeralSystem, long expectedValue) {
        long convertedValue = NumeralSystemConverter.convertToNumberSystem(value, numeralSystem);
        if (convertedValue == expectedValue) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testIllegalArgumentFailure(int value, int numeralSystem) {
        try {
            NumeralSystemConverter.convertToNumberSystem(value, numeralSystem);
            System.out.println(TEST_FAILED);
        } catch (IllegalArgumentException e) {
            System.out.println(TEST_COMPLETED);
        }
    }
}
