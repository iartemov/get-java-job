package ru.iartemov.gjj;

public class NumberComparator {
    public static boolean isBothNotEven(int x, int y) {
        boolean isXNotEven = x % 2 != 0;
        boolean isYNotEven = y % 2 != 0;
        return isXNotEven && isYNotEven;
    }

    public static boolean isOnlyOneLessThan20(int x, int y) {
        boolean isXLessThan20 = x < 20;
        boolean isYLessThan20 = y < 20;
        return isXLessThan20 ^ isYLessThan20;
    }

    public static boolean isAtLeastOneEqualZero(int x, int y) {
        boolean isXEqualZero = x == 0;
        boolean isYEqualZero = y == 0;
        return isXEqualZero || isYEqualZero;
    }

    public static boolean isAllNegative(int x, int y, int z) {
        boolean isXNegative = x < 0;
        boolean isYNegative = y < 0;
        boolean isZNegative = z < 0;
        return isXNegative && isYNegative && isZNegative;
    }

    public static boolean isOnlyOneDivisibleBy5(int x, int y, int z) {
        boolean isXDivisibleBy5 = x % 5 == 0;
        boolean isYDivisibleBy5 = y % 5 == 0;
        boolean isZDivisibleBy5 = z % 5 == 0;
        return (isXDivisibleBy5 ^ isYDivisibleBy5 ^ isZDivisibleBy5)
                && !(isXDivisibleBy5 && isYDivisibleBy5 && isZDivisibleBy5);
    }

    public static boolean isAtLeastOneMoreThanHundred(int x, int y, int z) {
        boolean isXMoreThanHundred = x > 100;
        boolean isYMoreThanHundred = y > 100;
        boolean isZMoreThanHundred = z > 100;
        return isXMoreThanHundred || isYMoreThanHundred || isZMoreThanHundred;
    }
}
