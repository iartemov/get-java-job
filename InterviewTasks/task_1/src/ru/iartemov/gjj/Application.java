package ru.iartemov.gjj;

public class Application {

    public static final String TEST_COMPLETED = "Test completed";
    public static final String TEST_FAILED = "Test failed";

    public static void main(String[] args) {
        System.out.println("First subtask test");
        testFirstSubtask(1, 1, true);
        testFirstSubtask(1, 2, false);
        testFirstSubtask(2, 1, false);
        testFirstSubtask(2, 2, false);

        System.out.println();

        System.out.println("Second subtask test");
        testSecondSubtask(1, 1, false);
        testSecondSubtask(21, 1, true);
        testSecondSubtask(1, 21, true);
        testSecondSubtask(21, 21, false);

        System.out.println();

        System.out.println("Third subtask test");
        testThirdSubtask(0, 0, true);
        testThirdSubtask(1, 0, true);
        testThirdSubtask(0, 1, true);
        testThirdSubtask(1, 1, false);

        System.out.println();

        System.out.println("Fourth subtask test");
        testFourthSubtask(1, 1, 1, false);
        testFourthSubtask(-1, 1, 1, false);
        testFourthSubtask(-1, -1, 1, false);
        testFourthSubtask(-1, -1, -1, true);

        System.out.println();

        System.out.println("Fifth subtask test");
        testFifthSubtask(1, 1, 1, false);
        testFifthSubtask(5, 1, 1, true);
        testFifthSubtask(5, 5, 1, false);
        testFifthSubtask(5, 5, 5, false);
        testFifthSubtask(1, 5, 5, false);
        testFifthSubtask(1, 1, 5, true);

        System.out.println();

        System.out.println("Sixth subtask test");
        testSixthSubtask(1, 1, 1, false);
        testSixthSubtask(101, 1, 1, true);
        testSixthSubtask(1, 101, 1, true);
        testSixthSubtask(1, 1, 101, true);
    }

    private static void testFirstSubtask(int x, int y, boolean expectedResult) {
        if (NumberComparator.isBothNotEven(x, y) == expectedResult) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testSecondSubtask(int x, int y, boolean expectedResult) {
        if (NumberComparator.isOnlyOneLessThan20(x, y) == expectedResult) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testThirdSubtask(int x, int y, boolean expectedResult) {
        if (NumberComparator.isAtLeastOneEqualZero(x, y) == expectedResult) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testFourthSubtask(int x, int y, int z, boolean expectedResult) {
        if (NumberComparator.isAllNegative(x, y, z) == expectedResult) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testFifthSubtask(int x, int y, int z, boolean expectedResult) {
        if (NumberComparator.isOnlyOneDivisibleBy5(x, y, z) == expectedResult) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void testSixthSubtask(int x, int y, int z, boolean expectedResult) {
        if (NumberComparator.isAtLeastOneMoreThanHundred(x, y, z) == expectedResult) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }
}
