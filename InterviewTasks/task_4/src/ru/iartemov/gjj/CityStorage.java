package ru.iartemov.gjj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CityStorage {
    private static List<String> cityNames;

    static {
        cityNames = Arrays.asList(
                "Moscow",
                "Murmansk",
                "Anapa",
                "Sochi",
                "Tagil",
                "Samara",
                "Omsk",
                "Petrozavodsk",
                "Kaluga",
                "Kem'",
                "Ivanovo",
                "Tomsk",
                "Ulyanovsk"
        );
    }

    public static List<String> getCityNamesWithStartingSubstring(String substring) {
        if (substring == null) {
            throw new IllegalArgumentException("Substring must be no null!");
        }

        List<String> filteredCityNames = new ArrayList<>();
        for (String name : cityNames) {
            String lowerCaseCityName = name.toLowerCase();
            String lowerCaseSubstring = substring.toLowerCase();
            if (lowerCaseCityName.startsWith(lowerCaseSubstring)) {
                filteredCityNames.add(name);
            }
        }
        return filteredCityNames;
    }
}
