package ru.iartemov.gjj;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Application {

    public static final String TEST_COMPLETED = "Test completed";
    public static final String TEST_FAILED = "Test failed";
    public static final String CITY_NOT_FOUND = "Cities with name starting from specified substring not found";

    public static void main(String[] args) {
        // Tests
        test("M", Arrays.asList("Moscow", "Murmansk"));
        test("sam", Arrays.asList("Samara"));
        test("mo", Arrays.asList("Moscow"));
        failureTest();

        // Run
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String substring = scanner.nextLine();
            List<String> filteredCityNames = CityStorage.getCityNamesWithStartingSubstring(substring);
            if (filteredCityNames.isEmpty()) {
                System.out.println(CITY_NOT_FOUND);
            } else {
                for (String cityName : filteredCityNames) {
                    System.out.println(cityName);
                }
            }
        }
    }

    private static void test(String substring, List<String> expectedResult) {
        List<String> actualResult = CityStorage.getCityNamesWithStartingSubstring(substring);
        if (actualResult.equals(expectedResult)) {
            System.out.println(TEST_COMPLETED);
        } else {
            System.out.println(TEST_FAILED);
        }
    }

    private static void failureTest() {
        try {
            CityStorage.getCityNamesWithStartingSubstring(null);
            System.out.println(TEST_FAILED);
        } catch (IllegalArgumentException e) {
            System.out.println(TEST_COMPLETED);
        }
    }
}
