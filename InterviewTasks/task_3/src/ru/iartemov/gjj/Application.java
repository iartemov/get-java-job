package ru.iartemov.gjj;

public class Application {
    public static void main(String[] args) {
        // First success test
        Team firstTeam = new Team("PG Jaguars");
        Team secondTeam = new Team("Washington Generals");

        BasketballGame game = new BasketballGame(firstTeam, secondTeam);
        game.makeGoal(firstTeam, 1);
        game.makeGoal(firstTeam, 3);
        game.makeGoal(secondTeam, 2);
        game.makeGoal(secondTeam, 2);
        game.makeGoal(firstTeam, 1);

        game.finishGame();

        // Test make goal after game finish
        game.makeGoal(firstTeam, 1);

        // Test illegal points value
        BasketballGame secondGame = new BasketballGame(firstTeam, secondTeam);
        try {
            secondGame.makeGoal(firstTeam, 1000);
        } catch (IllegalArgumentException e) {
            System.out.println("Test complete");
        }
    }
}
