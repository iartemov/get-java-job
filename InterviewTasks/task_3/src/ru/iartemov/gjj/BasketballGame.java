package ru.iartemov.gjj;

public class BasketballGame {
    public static final String ILLEGAL_POINT_VALUE_EXC_MSG = "Points must be between 0 and 3";
    private Team firstTeam;
    private Team secondTeam;
    private int firstTeamScore;
    private int secondTeamScore;
    private boolean isFinished;

    public BasketballGame(Team firstTeam, Team secondTeam) {
        this.firstTeam = firstTeam;
        this.secondTeam = secondTeam;
    }

    public void makeGoal(Team team, int points) {
        if (isFinished) {
            System.out.println("Game is finished!");
            return;
        }

        checkPointsValue(points);

        if (team.equals(firstTeam)) {
            firstTeamScore += points;
        } else if (team.equals(secondTeam)) {
            secondTeamScore += points;
        } else {
            return;
        }
        System.out.println("Team " + team.getName() + " make goal with " + points + " points!");
        showScore();
    }

    private void checkPointsValue(int points) {
        if (points < 0 || points > 3) {
            throw new IllegalArgumentException(ILLEGAL_POINT_VALUE_EXC_MSG);
        }
    }

    public void showScore() {
        StringBuilder builder = new StringBuilder();
        builder.append("Score : ");
        builder.append(firstTeam.getName());
        builder.append(" ");
        builder.append(firstTeamScore);
        builder.append(" : ");
        builder.append(secondTeamScore);
        builder.append(" ");
        builder.append(secondTeam.getName());
        System.out.println(builder.toString());
    }

    public void finishGame() {
        isFinished = true;
        showScore();
        Team winner = getWinner();
        if (winner != null) {
            System.out.println("Team " + winner.getName() + " win!");
        } else {
            System.out.println("Stand-off!");
        }
    }

    public Team getWinner() {
        if (firstTeamScore > secondTeamScore) {
            return firstTeam;
        } else if (secondTeamScore > firstTeamScore) {
            return secondTeam;
        } else {
            return null;
        }
    }
}
